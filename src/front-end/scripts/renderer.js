import {VgsCircle} from "./canvas/vgsCircle.js"
import {InnerCircleAnimation} from "./canvas/animation/innerCircleAnimation.js"
import {Preference} from "./canvas/preference.js"

renderCanvas()

function renderCanvas() {
  if (window.jsonError) {
    alert(window.jsonError)
    window.close()
  }

  const preference = new Preference(window.json)
  const vgs = new VgsCircle(preference)
  const innerCircle = new InnerCircleAnimation(preference)

  document.addEventListener('keyup', keyup)
  document.addEventListener('keydown', keydown)
  document.addEventListener('mousedown', mousedown)
  document.addEventListener('mousemove', mousemove)

  function keydown(event) {
    if (vgs.allowedKeys.includes(event.key.toLowerCase()) || vgs.allowedKeys.includes(event.key.toUpperCase())) {
      let jsonKey = getJsonKeyFromKey(event.key)
      if (jsonKey === vgs.pressedKey) return
      vgs.pressedKey = jsonKey
      vgs.animationHover(jsonKey).then(r => r)
    }
  }

  function keyup(event) {
    if (event.key === 'Escape') {
      if (vgs.pressedKey !== null) {
        vgs.pressedKey = null
        vgs.animationHover(null).then(r => r)
        return
      }
      innerCircle.startRotation(false)
      handleEscape()
    }

    if ((vgs.allowedKeys.includes(event.key.toLowerCase()) || vgs.allowedKeys.includes(event.key.toUpperCase()) && vgs.pressedKey !== null)) {
      innerCircle.startRotation(true)
      vgs.pressedKey = null
      handleKeys(event.key).then(r => r)
    }
  }

  /**aaa
   * close the window if we are in the 1st level of the json
   * else return on the parent level
   */
  function handleEscape() {
    if (vgs.pickedJsonKeys.length === 0) {
      window.api.send('minimize')
    }
    let tmp = vgs.shortcuts
    vgs.pickedJsonKeys.pop()
    for (let i = 0; i < vgs.pickedJsonKeys.length; i++) {
      tmp = tmp[vgs.pickedJsonKeys[i]]
    }
    vgs.currentJsonStep = tmp
    vgs.allowedKeys = []
    Object.keys(vgs.currentJsonStep).forEach(element => {
      vgs.allowedKeys.push(element.split("]")[0][1])
    })
    vgs.drawVGS()
  }

  /**
   * handle currentJsonStep on key input
   * if the json related key is a string we execute the command
   * else we go deeper in the category
   * @param key
   */
  async function handleKeys(key) {
    let jsonKey = getJsonKeyFromKey(key)
    let indexJsonKey = getJsonIndexFromKey(key)

    if (jsonKey === null) return

    // we chose a category
    if (vgs.currentJsonStep[jsonKey] instanceof Object) {
      await vgs.animationChosenKey(indexJsonKey, jsonKey, true)
    }
    // we chose an action to execute
    else {
      await vgs.animationChosenKey(indexJsonKey, jsonKey, false)
      window.api.send('command-endpoint', vgs.currentJsonStep[jsonKey])
      vgs.init()
    }
  }

  /**
   * handle click on document
   * @param event
   */
  function mousedown(event) {
    let cRect = vgs.canvas.getBoundingClientRect()
    let mouseX = Math.round(event.clientX - cRect.left)
    let mouseY = Math.round(event.clientY - cRect.top)

    if (distance(vgs.centerX, vgs.centerY, mouseX, mouseY) < vgs.radius / vgs.preference.innerCircleRatio) {
      handleEscape()
    } else {
      mousedownSection(mouseX, mouseY)
    }
  }

  /**
   * handle mouse move on document
   * @param event
   */
  function mousemove(event) {
    let cRect = vgs.canvas.getBoundingClientRect()
    let mouseX = Math.round(event.clientX - cRect.left)
    let mouseY = Math.round(event.clientY - cRect.top)


    let hovered = hoveredSection(mouseX, mouseY)

    if (hovered?.key !== vgs.hoveredSection?.key) {
      vgs.hoveredSection = hovered
      if (distance(vgs.centerX, vgs.centerY, mouseX, mouseY) < vgs.radius / vgs.preference.innerCircleRatio) {
        vgs.animationHover('inner').then(r => r)
        return
      }
      vgs.animationHover(vgs.hoveredSection?.key).then(r => r)
    }
  }

  /**
   * calculate the distance between to points in pixel
   * @param x
   * @param y
   * @param a
   * @param b
   */
  function distance(x, y, a, b) {
    return Math.sqrt(Math.pow(x - a, 2) + Math.pow(y - b, 2))
  }

  /**
   * handle click on one section
   * @param mouseX
   * @param mouseY
   */
  function mousedownSection(mouseX, mouseY) {
    let hovered = hoveredSection(mouseX, mouseY)
    if (hovered) {
      handleKeys(hovered.key.split("]")[0][1]).then(r => r)
    }
  }

  /**
   * handle mouse hover on one section
   * @param mouseX
   * @param mouseY
   */
  function hoveredSection(mouseX, mouseY) {
    if (distance(vgs.centerX, vgs.centerY, mouseX, mouseY) > vgs.radius - 10) {
      return null
    }

    if (distance(vgs.centerX, vgs.centerY, mouseX, mouseY) < vgs.radius / vgs.preference.innerCircleRatio) {
      return null
    }

    let AB = Math.sqrt(Math.pow(vgs.centerX - mouseX, 2) + Math.pow(vgs.centerY - mouseY, 2))
    let BC = Math.sqrt(Math.pow(vgs.centerX - vgs.centerX, 2) + Math.pow(vgs.centerY - 0, 2))
    let AC = Math.sqrt(Math.pow(vgs.centerX - mouseX, 2) + Math.pow(0 - mouseY, 2))
    let angle = Math.acos((BC * BC + AB * AB - AC * AC) / (2 * BC * AB)) * (180 / Math.PI)

    if (mouseX < vgs.radius) {
      const rst = 180 - angle
      angle = rst + 180
    }

    return vgs.sections.find(section => {
      if (section.start === section.end) return true
      if (section.start > section.end) {
        return (section.start >= angle && 0 <= angle) || (section.end <= angle && 360 >= angle)
      }
      return (section.end >= angle && section.start <= angle)
    })
  }

  function getJsonKeyFromKey(key) {
    for (let indexJsonKey = 0; indexJsonKey < Object.keys(vgs.currentJsonStep).length; indexJsonKey++) {
      if (!(key.toLowerCase() === vgs.allowedKeys[indexJsonKey].toLowerCase())) continue
      return Object.keys(vgs.currentJsonStep)[indexJsonKey].toString()
    }
    return null
  }

  function getJsonIndexFromKey(key) {
    for (let indexJsonKey = 0; indexJsonKey < Object.keys(vgs.currentJsonStep).length; indexJsonKey++) {
      if (!(key.toLowerCase() === vgs.allowedKeys[indexJsonKey].toLowerCase())) continue
      return indexJsonKey
    }
    return -1
  }
}

// when click on body and not on the wrapper minimize the window
document.getElementById("body").addEventListener("click", function () {
  window.api.send('minimize')
})

document.getElementById("wrapper").addEventListener("click", function (event) {
  event.stopPropagation()
})

/**
 * this class handle the little moving circle in the center of the wheel
 */
export class InnerCircleAnimation {

  constructor(preference) {
    this.preference = preference
    this.canvas = document.getElementById("animationCanvas")
    this.canvas.width = this.preference.size
    this.canvas.height = this.preference.size
    this.context = this.canvas.getContext("2d")
    this.startAngle = (2 * Math.PI)
    this.speed = 0
    this.counterClockwise = true
    this.enableRotation = false
    this.endAngle = (Math.PI * 1.75)
    this.currentAngle = 0

    this.update()

  }

  /**
   * do the rotation of the arc
   */
  update() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)

    if (this.enableRotation && (this.preference.enableAnimation)) {
      this.context.beginPath()
      this.context.arc(this.canvas.width / 2, this.canvas.height / 2, this.canvas.height / (this.preference.innerCircleRatio * 2), this.startAngle + this.currentAngle, this.endAngle + this.currentAngle, false)
      this.context.strokeStyle = this.preference.colorBorder
      this.context.lineWidth = 5
      this.context.stroke()

      this.rotate()
    }

    this.sleep(25).then(() => {
      this.update()
    })
  }

  /**
   * sleep to fake frame rate of the screen
   * @param ms
   * @returns {Promise<unknown>}
   */
  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
  }

  /**
   * start the rotation of the arc
   * @param counterClockwise
   */
  startRotation(counterClockwise) {
    this.counterClockwise = counterClockwise
    this.enableRotation = true
    this.speed = 0
  }

  /**
   * this function change the pace of the arc
   */
  rotate() {
    this.currentAngle += this.speed

    if (this.speed > 0.4) {
      this.enableRotation = false
    }
    if (this.speed < -0.4) {
      this.enableRotation = false
    }
    if (this.counterClockwise) {
      this.speed += 0.015
    } else {
      this.speed -= 0.015
    }
  }
}

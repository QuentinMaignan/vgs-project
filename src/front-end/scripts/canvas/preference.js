export class Preference {
  constructor(jsonConf) {
    this.colorBorder = jsonConf.style?.color?.border ?? 'black'
    this.colorText = jsonConf.style?.text?.color ?? 'black'
    this.textBorder = jsonConf.style?.text?.border ?? false
    this.colorTextBorder = jsonConf.style?.text?.borderColor ?? "black"
    this.outerCircleBackground = jsonConf.style?.color?.outerCircleBackground ?? 'white'
    this.outerCircleBackgroundSelected = jsonConf.style?.color?.outerCircleBackgroundSelected ?? 'grey'
    this.innerCircleBackground = jsonConf.style?.color?.innerCircleBackground ?? 'grey'
    this.innerCircleBackgroundSelected = jsonConf.style?.color?.innerCircleBackgroundSelected ?? 'red'
    this.outerCircleBackgroundGradient = jsonConf.style?.color?.outerCircleBackgroundGradient ?? "false"
    this.outerCircleBackgroundGradientColor1 = jsonConf.style?.color?.outerCircleBackgroundGradientColor1 ?? "rgba(0, 0, 255, 0.5)"
    this.outerCircleBackgroundGradientColor2 = jsonConf.style?.color?.outerCircleBackgroundGradientColor2 ?? "rgba(0, 0, 255, 0)"
    this.outerCircleBackgroundGradientColorSelected1 = jsonConf.style?.color?.outerCircleBackgroundGradientColorSelected1 ?? "rgba(255, 0, 0, 0.5)"
    this.outerCircleBackgroundGradientColorSelected2 = jsonConf.style?.color?.outerCircleBackgroundGradientColorSelected2 ?? "rgba(255, 0, 0, 0)"
    this.size = jsonConf.style?.dimension?.size ?? 600
    this.font = jsonConf.style?.text?.font ?? '25px Lato'
    this.enableAnimation = jsonConf.style?.enableAnimation ?? true
    this.shiftStartingAngle = jsonConf.style?.shiftStartingAngle ?? true
    this.innerCircleRatio = jsonConf.style?.dimension?.innerCircleRatio ?? 4
    this.innerCircleImage = jsonConf.style?.innerCircleImage ?? true
    this.drawBorder = jsonConf.style?.drawBorder ?? true
  }
}

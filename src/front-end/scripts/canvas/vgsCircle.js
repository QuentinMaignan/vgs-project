/**
 * Vgs circle handle the wheel draw
 */
export class VgsCircle {
  constructor(preference) {
    let jsonConf = window.json

    this.preference = preference
    this.shortcuts = jsonConf.shortcuts
    this.currentJsonStep = {}
    this.allowedKeys = []
    this.pickedJsonKeys = []
    this.sections = []
    this.hoveredSection = null
    this.pressedKey = null
    this.canvas = document.getElementById("circleCanvas")
    this.canvas.width = this.preference.size
    this.canvas.height = this.preference.size

    this.context = this.canvas.getContext('2d')
    this.centerX = this.canvas.width / 2
    this.centerY = this.canvas.height / 2
    this.radius = this.canvas.height / 2

    this.keydown = false

    this.context.font = this.preference.font
    this.init()
  }

  init() {
    this.currentJsonStep = this.shortcuts
    this.addAllowKeys()
    this.pickedJsonKeys = []

    this.drawVGS()
  }

  /**
   * check the current json step to set all allowed keys
   */
  addAllowKeys() {
    this.allowedKeys = []
    Object.keys(this.currentJsonStep).forEach(element => {
      this.allowedKeys.push(element.split("]")[0][1])
    })
  }

  /**
   * draw every part of the wheel
   * @param innerSelected
   */
  drawVGS(innerSelected = false) {
    if (!this.canvas.getContext) return
    this.clearCanvas()
    this.setSections()
    this.drawOuterCircle()
    this.drawInnerCircle(innerSelected)
  }

  /**
   * draw the Outer circle, the text or icon, and the key to select the section
   * @param chosenSection
   */
  drawOuterCircle(chosenSection) {
    let offset = Math.PI / 2
    let arcSector = Math.PI * (2 * (100 / Object.keys(this.currentJsonStep).length) / 100)
    let lastEnd = this.preference.shiftStartingAngle ? arcSector / 2 : 0

    for (let section = 0; section < Object.keys(this.currentJsonStep).length; section++) {
      let color = this.setColorForSection(chosenSection, section)
      this.drawOuterCircleSection(lastEnd, offset, arcSector, color)
      this.writeTextAndIcon(lastEnd, arcSector, offset, section)

      lastEnd += arcSector
    }
  }

  /**
   * the color depend on if the section is selected or not and if we use gradient or normal color
   * @param chosenSection
   * @param section
   * @returns {*}
   */
  setColorForSection(chosenSection, section) {
    let color = this.preference.outerCircleBackground
    if (this.preference.outerCircleBackgroundGradient) {
      let gradient = this.context.createRadialGradient(this.centerX, this.centerY, 0, this.centerX, this.centerY, this.radius)
      gradient.addColorStop(0, this.preference.outerCircleBackgroundGradientColor1)
      gradient.addColorStop(1, this.preference.outerCircleBackgroundGradientColor2)
      color = gradient
    }

    if (chosenSection !== undefined && section === chosenSection) {
      color = this.preference.outerCircleBackgroundSelected
      if (this.preference.outerCircleBackgroundGradient) {
        let gradient = this.context.createRadialGradient(this.centerX, this.centerY, 0, this.centerX, this.centerY, this.radius)
        gradient.addColorStop(0, this.preference.outerCircleBackgroundGradientColorSelected1)
        gradient.addColorStop(1, this.preference.outerCircleBackgroundGradientColorSelected2)
        color = gradient
      }
    }
    return color
  }

  /**
   * init the section objects to calculate the angle of each sections
   */
  setSections() {
    this.sections = []

    let arcSector = Math.PI * (2 * (100 / Object.keys(this.currentJsonStep).length) / 100)
    let lastEnd = this.preference.shiftStartingAngle ? arcSector / 2 : 0

    for (let section = 0; section < Object.keys(this.currentJsonStep).length; section++) {
      const sectionElement = {
        key: Object.keys(this.currentJsonStep)[section],
        start: (lastEnd) * (180 / Math.PI),
        end: ((lastEnd + arcSector) * (180 / Math.PI)) % 360
      }
      lastEnd += arcSector
      this.sections.push(sectionElement)
    }
  }

  /**
   * draw only one section of the wheel
   * @param lastEnd
   * @param offset
   * @param arcSector
   * @param color
   */
  drawOuterCircleSection(lastEnd, offset, arcSector, color) {
    this.context.beginPath()
    this.context.moveTo(this.centerX, this.centerY)
    this.drawArc(this.centerX, this.centerY, this.radius, lastEnd - offset, lastEnd + arcSector - offset, color)
    this.context.closePath()
  }

  /**
   * write the text and key on the wheel
   * @param lastEnd
   * @param arcSector
   * @param offset
   * @param section
   */
  writeTextAndIcon(lastEnd, arcSector, offset, section) {
    let key = Object.keys(this.currentJsonStep)[section].split("]")[0][1]
    const metrics = this.context.measureText(key)
    const textHeight = metrics.fontBoundingBoxAscent + metrics.fontBoundingBoxDescent
    const cos = Math.cos(lastEnd + arcSector / 2 + Math.PI + offset)
    const sin = Math.sin(lastEnd + arcSector / 2 + Math.PI + offset)

    this.writeKey(metrics, cos, textHeight, sin, key)
    this.writeTextOrImage(section, cos, sin, textHeight)
  }

  /**
   * write the text which in the config, if the text is an existing path then draw the icon
   * @param section
   * @param cos
   * @param sin
   * @param textHeight
   */
  writeTextOrImage(section, cos, sin, textHeight) {
    let textOrPath = Object.keys(this.currentJsonStep)[section].split("]")[1].trim()
    let startTextX = this.centerX + this.radius * 2.75 / 4 * cos
    let startTextY = (this.centerY + this.radius * 3 / 4 * sin) + textHeight / 3
    let image = new Image()
    image.onload = () => {
      image.width = this.canvas.width * 120 / 600
      image.height = this.canvas.height * 120 / 600
      let width = image.width
      let height = image.height
      this.context.drawImage(image, startTextX - width / 2, startTextY - height / 2, width, height)
    }

    image.src = textOrPath
    if (!image.complete) {
      const textCategory = textOrPath.length < 12 ? textOrPath : textOrPath.substring(0, 9) + "..."
      this.writeText(startTextX, startTextY, textCategory)
    }
  }

  /**
   * draw the key to activate a section
   * @param metrics
   * @param cos
   * @param textHeight
   * @param sin
   * @param key
   */
  writeKey(metrics, cos, textHeight, sin, key) {
    const startKeyX = (this.radius / (this.preference.innerCircleRatio) + metrics.width) * cos
    const startKeyY = ((this.radius / (this.preference.innerCircleRatio) + textHeight) * sin) + textHeight / 2
    this.writeText(this.centerX + startKeyX, this.centerY + startKeyY, key)
  }

  /**
   * draw the little circle in the big circle with image or not depending on the config file
   * @param innerSelected
   */
  drawInnerCircle(innerSelected = false) {
    let color = this.preference.innerCircleBackground
    if (innerSelected) {
      color = this.preference.innerCircleBackgroundSelected
    }
    this.clearCircle(this.centerX, this.centerY, this.radius / this.preference.innerCircleRatio)
    this.context.beginPath()
    this.drawArc(this.centerX, this.centerY, this.radius / this.preference.innerCircleRatio, 0, 2 * Math.PI, color)
    this.context.closePath()
    if (this.preference.innerCircleImage) {
      this.drawInnerImage()
    }
  }

  /**
   * chose the right icon in the inner circle
   */
  drawInnerImage() {
    let image = new Image()
    image.onload = () => {
      this.context.drawImage(image, this.centerX - this.canvas.width / (this.preference.innerCircleRatio * 2), this.centerY - this.canvas.height / (this.preference.innerCircleRatio * 2), this.canvas.width / this.preference.innerCircleRatio, this.canvas.height / this.preference.innerCircleRatio)
    }
    if (this.pickedJsonKeys.length === 0) {
      image.src = "../assets/icons/close.svg"
    } else {
      image.src = "../assets/icons/backarrow.svg"
    }
  }

  /**
   * draw arc in the given position
   * @param x
   * @param y
   * @param r
   * @param start
   * @param end
   * @param color
   */
  drawArc(x, y, r, start, end, color) {
    this.context.arc(x, y, r, start, end, false)
    this.context.strokeStyle = this.preference.colorBorder
    if (this.preference.drawBorder) {
      this.context.stroke()
    }
    this.context.fillStyle = color

    this.context.fill()
  }


  /**
   * write text at given position
   * @param x
   * @param y
   * @param text
   */
  writeText(x, y, text) {
    this.context.textAlign = "center"
    this.context.fillStyle = this.preference.colorText
    this.context.fillText(text, x, y)
    this.context.strokeStyle = this.preference.colorTextBorder
    if (this.preference.textBorder)
      this.context.strokeText(text, x, y)
  }

  /**
   * launch the animation, and update the keys if updateKey is true
   * @param indexChosen
   * @param jsonKey
   * @param updateKey
   * @returns {Promise<void>}
   */
  async animationChosenKey(indexChosen, jsonKey, updateKey) {
    this.drawOneSection(indexChosen)
    if (updateKey) {
      this.selectNextJsonKey(jsonKey)
    }
    await this.sleep(250).then(r => r)
    if (!updateKey) {
      return
    }
    this.drawVGS()
  }

  /**
   * animation when we hover the circle with the mouse
   * @param jsonKey
   * @returns {Promise<void>}
   */
  async animationHover(jsonKey) {
    if (!this.preference.enableAnimation)
      return
    if (!jsonKey) {
      this.drawVGS()
      return
    }
    if (jsonKey === 'inner') {
      this.drawVGS(true)
      return
    }
    let jsonKeyIndex = Object.keys(this.currentJsonStep).findIndex(jsonStepKey => jsonStepKey === jsonKey)
    this.drawOneSection(jsonKeyIndex)
  }

  /**
   * draw one section of the wheel
   * @param indexChosen
   */
  drawOneSection(indexChosen) {
    if (!this.preference.enableAnimation) return
    this.clearCanvas()
    this.drawOuterCircle(indexChosen)
    this.drawInnerCircle()
  }

  /**
   * giving a key we go deeper in the json
   * @param jsonKey
   */
  selectNextJsonKey(jsonKey) {
    this.pickedJsonKeys[this.pickedJsonKeys.length] = jsonKey
    this.currentJsonStep = this.currentJsonStep[jsonKey]
    this.addAllowKeys()
  }

  /**
   * clear the whole canvas
   */
  clearCanvas() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
  }

  /**
   * clear a circle in the given position
   * @param x
   * @param y
   * @param radius
   */
  clearCircle(x, y, radius) {
    this.context.save()
    this.context.beginPath()
    this.context.arc(x, y, radius, 0, 2 * Math.PI, false)
    this.context.clip()
    this.context.clearRect(x - radius, y - radius, radius * 2, radius * 2)
    this.context.restore()
  }

  /**
   * wait for X ms
   * @param ms
   * @returns {Promise<unknown>}
   */
  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
  }

}

import {contextBridge, ipcRenderer} from "electron"
const parseJson = require('json-parse-better-errors')

// send + receive msg
contextBridge.exposeInMainWorld('api', {
  send: (channel, data) => ipcRenderer.invoke(channel, data),
  handle: (channel, callable, event, data) => ipcRenderer.on(channel, callable(event, data))
})


getJsonConf().then()

/**
 * if the object or any of his children have more than 8 keys we raised an error
 * @param obj
 * @param firstLevel
 * @returns {null|*}
 */
function checkLength(obj, firstLevel = true) {
  let check = null
  if (typeof obj === 'string') {
    return check
  }
  if (Object.keys(obj).length > 8) return obj
  for (const objKey in obj) {
    if (!firstLevel || (firstLevel && objKey === 'shortcuts')) {
      check = checkLength(obj[objKey], false) ?? check
    }
  }
  return check
}

function checkKeys(obj, firstLevel = true) {
  let check = null
  if (typeof obj === 'string') {
    return check
  }

  let keys = []
  Object.keys(obj).forEach(element => {
    keys.push(element.split("]")[0][1].toUpperCase())
  })

  if (keys.length !== new Set(keys).size) return obj
  for (const objKey in obj) {
    if (!firstLevel || (firstLevel && objKey === 'shortcuts')) {
      check = checkKeys(obj[objKey], false) ?? check
    }
  }
  return check
}

/**
 * check if the json file for configuration is good
 * @param jsonConf
 * @returns {string|null}
 */
function checkJsonConf(jsonConf) {
  let errorLength = checkLength(jsonConf)
  if (errorLength) return `To many categories mapped in [${Object.keys(errorLength).join(', ')}] maximum is 8 categories, you mapped ${Object.keys(errorLength).length} categories`
  let errorKeys = checkKeys(jsonConf.shortcuts, false)
  if (errorKeys) return `Some keys in [${Object.keys(errorKeys).join(', ')}] are used more than once please replace it with another one`
  return null
}

/**
 * get or create the json conf file
 * @returns {Promise<void>}
 */
async function getJsonConf() {
  const fs = require('fs')
  contextBridge.exposeInMainWorld('fs', fs)

  const homedir = require('os').homedir()
  let homeConfPath = `${homedir}/.vgsConfig.json`

  try {
    let file = fs.readFileSync(homeConfPath, 'utf8')
    let jsonConf = parseJson(file)
    let error = checkJsonConf(jsonConf)
    if (error) {
      console.log(error)
      contextBridge.exposeInMainWorld('jsonError', error)
    }
    contextBridge.exposeInMainWorld('json', jsonConf)
  } catch (e) {
    contextBridge.exposeInMainWorld('jsonError', e.message)
  }
}

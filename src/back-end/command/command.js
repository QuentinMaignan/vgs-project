import {exec} from "child_process"

/**
 * this class will handle command, either with keys using
 */
export class Command {

  constructor(command) {
    if (command.includes('[SHORTCUT]')) {
      command = command.replaceAll(' ', '')
      command = command.replaceAll('[SHORTCUT]', '')
      this.keys = command
      this.executeXte()
    } else if (command.includes('[COMMAND]')) {
      command = command.replaceAll('[COMMAND]', '')
      this.execute(command)
    }
  }

  /**
   * fake a combination of keys
   */
  executeXte() {
    let xteKeys = this.keys.split('+')
    for (let i = 0; i < xteKeys.length; i++) {
      xteKeys[i] = this.translateKeyForXte(xteKeys[i])
    }

    let command = `xte `
    let keydown = ``
    let keyup = ``
    xteKeys.forEach(key => {
      keydown += `'keydown ${key}' `
      keyup += `'keyup ${key}' `
    })
    command = command + keydown + keyup

    this.execute(command)
  }

  /**
   * simply execute command in node js
   * @param command
   */
  execute(command) {
    exec(command, (error, stdout, stderr) => {
      if (error) {
        console.log(`error: ${error.message}`)
        return
      }
      if (stderr) {
        console.log(`stderr: ${stderr}`)
        return
      }
      console.log(`stdout: ${stdout}`)
    })
  }

  /**
   * in order to make xautomation work we need to pass the good keys,
   * this function help user to pass easiest keys
   * @param key
   * @returns {*}
   */
  translateKeyForXte(key) {
    // key => value
    // key is the expected string for xte
    // value is all the string contains inside the json which will be mapped to the key
    let dictKeys = {
      "Home": ['home'],
      "Left": ['left'],
      "Up": ['up'],
      "Right": ['right'],
      "Down": ['down'],
      "Page_Up": ['page_up'],
      "Page_Down": ['page_down'],
      "End": ['end'],
      "Return": ['return'],
      "BackSpace": ['backspace'],
      "Tab": ['tab'],
      "Escape": ['escape'],
      "Delete": ['delete', 'del'],
      "Shift_L": ['shift'],
      "Shift_R": ['shift_r'],
      "Control_L": ['control', 'ctrl'],
      "Control_R": ['control', 'ctrl'],
      "Meta_L": ['meta'],
      "Meta_R": ['meta_r'],
      "Alt_L": ['alt'],
      "Alt_R": ['alt_r'],
      "Multi_key": ['multi_key'],
      "Super_L": ['super'],
      "Super_R": ['super_r'],
    }

    let newKey = key
    Object.keys(dictKeys).forEach(dictKey => {
      let keyMapping = Object.values(dictKeys[dictKey]).map(i => i.toLowerCase())
      if (keyMapping.includes(key.toLowerCase())) {
        newKey = dictKey
      }
    })

    return newKey
  }
}

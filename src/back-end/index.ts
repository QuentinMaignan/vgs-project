import {app, BrowserWindow, contextBridge, globalShortcut, ipcMain, Menu, Tray} from 'electron'
import * as path from 'path'
import {Command} from './command/command'

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) {
    app.quit()
}

const createWindow = async (): Promise<void> => {
    let jsonConf = await getJsonConf()
    startUpLaunch(jsonConf)
    const mainWindow = createMainWindow(false, jsonConf)

    addTray(mainWindow)
    registerShortCuts(mainWindow, jsonConf)
    handleIpcMessages(mainWindow)
}

/**
 * if autostart in config is true and the file is not already existing we create it
 * @param jsonConf
 */
function startUpLaunch(jsonConf: any) {
    const fs = require('fs')
    const homedir = require('os').homedir()
    const pathHomeDesktop = `${homedir}/.config/autostart/vgs.desktop`
    const baseConfPath = path.join(__dirname, '../../vgs.desktop')

    if (!fs.existsSync(pathHomeDesktop) && (jsonConf?.autoStart ?? true)) {
        fs.copyFile(baseConfPath, pathHomeDesktop, fs.constants.COPYFILE_EXCL, (err: any) => {
        })
    }
    if (fs.existsSync(pathHomeDesktop) && !(jsonConf?.autoStart ?? true)) {
        fs.unlinkSync(pathHomeDesktop)
    }
}

/**
 * create the main windows with all the needed parameters
 */
function createMainWindow(debug: boolean, jsonConf: any) {
    const mainWindow = new BrowserWindow({
        webPreferences: {
            preload: path.join(__dirname, "preload.js")
        },
        height: debug ? 800 : jsonConf?.style?.dimension?.size ?? 600,
        width: debug ? 1000 : jsonConf?.style?.dimension?.size ?? 600,
        transparent: true,
        frame: debug,
    })

    mainWindow.loadFile(path.join(__dirname, '../../src/front-end/views/index.html')).then()
    if (debug) {
        mainWindow.webContents.openDevTools()
    }
    //Hide icon in taskbar
    mainWindow.setSkipTaskbar(true)

    if (jsonConf?.hideAtStart ?? true) {
        mainWindow.minimize()
    }

    return mainWindow
}

/**
 * hide or show the window
 * @param browserWindow
 */
function toggleVisibility(browserWindow: BrowserWindow) {
    if (browserWindow.isFocused()) {
        browserWindow.hide()
    } else if (!browserWindow.isFocused()) {
        browserWindow.show()
        browserWindow.focus()
    }
}

/**
 * add icon in the tray section, and add actions
 * @param mainWindow
 */
function addTray(mainWindow: Electron.CrossProcessExports.BrowserWindow) {
    let tray = new Tray(path.join(__dirname, '../../src/front-end/assets/icons/logoVGS_white.png'))
    const menu = Menu.buildFromTemplate([
        {
            label: 'Quit',
            click() {
                app.quit()
            }
        },
        {
            label: 'Show/Hide',
            click() {
                toggleVisibility(mainWindow)
            }
        },
    ])
    tray.setContextMenu(menu)
}

/**
 * add a global shortcut to show or hide the window (shortcut is executed even if the window is not focus)
 * @param mainWindow
 * @param jsonConf
 */
function registerShortCuts(mainWindow: Electron.CrossProcessExports.BrowserWindow, jsonConf: any) {
    globalShortcut.register(jsonConf?.showHideShortcut ?? 'CommandOrControl+F1', () => {
        toggleVisibility(mainWindow)
    })
}

/**
 * get json conf or null if the file doesn't exist
 */
async function getJsonConf() {
    const fs = require('fs')
    const homedir = require('os').homedir()
    const homeConfPath = `${homedir}/.vgsConfig.json`
    let baseConfPath = path.join(__dirname, '../../conf.json')

    if (!fs.existsSync(homeConfPath)) {
        await fs.copyFile(baseConfPath, homeConfPath, fs.constants.COPYFILE_EXCL, () => {
            try {
                return JSON.parse(fs.readFileSync(homeConfPath, 'utf8'))
            } catch (e) {
                return null
            }
        })
    }
}

/**
 * handle message receive from the front
 * @param mainWindow
 */
function handleIpcMessages(mainWindow: Electron.CrossProcessExports.BrowserWindow) {
    ipcMain.handle('command-endpoint', async (event, data) => {
        mainWindow.hide()
        new Command(data)
    })

    ipcMain.handle('minimize', async () => {
        mainWindow.hide()
    })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
    }
})

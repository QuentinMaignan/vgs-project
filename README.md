﻿# VGS - Wheel of Shortcuts
### Oussama ICHOUA - Quentin MAIGNAN - Nicolas MARTINEZ

![ShortcutWheel](README_Images/ShortCutWheel.png)
*Wheel of shortcuts*
# Introduction

Within the context of an end-of-year project carried out in Master 2 computer science (University Angers - France), we had to realize a system of shortcuts allowing to execute scripts by being inspired by the VGS system of the video game Paladins. In fact, this system of shortcuts allows you to quickly communicate with other players. It's interesting to find a system based on this principle of fast access to execute different scripts.

 ![VGS_Paladins](README_Images/VGS_Paladins.webp)
*Example of VGS from video game Paladins*

## How to install

To install it, please follow these instructions after downloading:

- download this deb file : [vgs_1.0.0_amd64.deb](https://drive.google.com/file/d/1pXGX9F9eI1ez-bEznOZWncCXCa3ZqmN8/view?usp=sharing)
> - **dpkg -i vgs_1.0.0_amd64.deb**
> - **sudo apt -f install** (to install dependent packages, xautomation)

## How to set up

After the installation you can open the wheel of shortcuts. *(By default: CTRL+F1 (GNOME) | CTRL+SHIFT+F1 (KDE+GNOME)).

To configure your shortcuts you will find in your **user directory** a json file allowing you to configure the shortcut wheel (**~/.vgsConfig.json** or **/home/*'username'*/.vgsConfig.json**)
When you are done after configuration you will have to close and reopen to see change or you can also use CTRL+R to reload.

### Authorized keys

In order to make your configuration actually working, you need to follow these steps
- separate keys with a +
- stand-alone keys do not have any change : a+b is correct, a  + B is correct
- all these keys are authorized : 
['home'],['left'],['up'],['right'],['down'],['page_up'],['page_down'],['end'],
['return'],['backspace'],['tab'],['escape'],['delete', 'del'],['shift'],['shift_r'],
['control', 'ctrl'],['meta'],['meta_r'],['alt'],['alt_r'],['multi_key'],['super'],['super_r'],
- good examples in the configuration file: 
  - [KEYS] control+a 
  - [KEYS] tab+B
  - [KEYS] shift+ctrl
- Bad examples in the configuration file:
    - [keys] ctrol a
    - [KEY] tab+BB
    - [KEYS] sHift+ctrl A

### Follow these rules to manage your json file

 1. You have to add "[A]" to choose key for each section, you can configure it at your own way.
 2. You only have 8 sections that can be divide into  8 sub-sections and again..
 3. You can set icons (Use png for better result - https://iconarchive.com/ ) if you replace name of sections with an absolute path
 3. If you made a mistake, the wheel will not open and tell you that there is an error in your file. (The error for sure will be in your json config file)
 4. You can also use https://jsonformatter.curiousconcept.com/ to check if there is no mistake, an omitted comma for example.

![exampleConfig](README_Images/Shortcut1.png)
*Example of the json config file*

## Autostart parameter
You will also find a parameter to choose if you want the wheel of shortcut already open when you start your computer
> "autoStart": true
> "autoStart": false

## How to change theme color
To change theme or color, it's at the end of the json file.
You can change all colors as you need, for the opacity it's in outerCircleBackground with rgba(RED,GREEN,BLUE,OPACITY) where you will find opacity value (1 is max and you will not see behind) 

You can also **enable/disable animations**.

![exampleConfig](README_Images/shortcut2.png)

### Follow these rules to manage your style configuration :
1. You can change all colors in your wheel
2. You can have a one colors in your wheel if you set 
> "outerCircleBackgroundGradient":false
3. If you set it at false, you can change background color with
> "outerCircleBackground": "rgba(19, 0, 207, 0.5)",
4. Use rgba to set opacity at the end
5. If you set outerCircleBackgroundGradient at true, you have to change these two parameters (and play with opacity to have a background totaly transparent)

>"outerCircleBackgroundGradientColor1":"rgba(255, 0, 0, 1)", <br>
>"outerCircleBackgroundGradientColor2":"rgba(255, 0, 0, 0)",

### Examples for styling - Lovingly created themes that you can apply!

**Smoky Rufous**
![smokyRufous](README_Images/themes/SmokyRufous.png)

>"color": { <br>
>      "border": "#BBC5AA", <br>
>      "text": "#BBC5AA", <br>
>      "outerCircleBackground": "rgba(167, 38, 8, 0.8)", <br>
>      "outerCircleBackgroundGradient":false, <br>
>      "outerCircleBackgroundGradientColor1":"rgba(255, 0, 0, 1)",<br> 
>      "outerCircleBackgroundGradientColor2":"rgba(255, 0, 0, 0)", <br>
>      "innerCircleBackground": "#DDE2C6", <br>
>      "selectedCategory": "#090C02" <br>
>          },

**Carrot Olive**
![carrotOlive](README_Images/themes/CarrotOlive.png)

>"color": { <br>
>      "border": "#EA9010", <br>
>      "text": "#EAEFBD", <br>
>      "outerCircleBackground": "rgba(55, 55, 31, 0.8)", <br>
>      "outerCircleBackgroundGradient":false, <br>
>      "outerCircleBackgroundGradientColor1":"rgba(255, 0, 0, 1)",<br> 
>      "outerCircleBackgroundGradientColor2":"rgba(255, 0, 0, 0)", <br>
>      "innerCircleBackground": "#90BE6D", <br>
>      "selectedCategory": "#EA9010" <br>
>          },

**Honey Charcoal**

![honeyCharcoal](README_Images/themes/HoneyCharcoal.png)

> "color": { <br>
>      "border": "#F6AE2D", <br>
>      "text": "#F6AE2D", <br>
>      "outerCircleBackground": "rgba(167, 38, 8, 0.8)", <br>
>      "outerCircleBackgroundGradient":true, <br>
>      "outerCircleBackgroundGradientColor1":"rgba(47, 72, 88, 0.8)", <br>
>      "outerCircleBackgroundGradientColor2":"rgba(51, 101, 138, 0)", <br>
>      "innerCircleBackground": "#86BBD8", <br>
>      "selectedCategory": "#F26419" <br>
>    },

**Hunter Citron**

![hunterCitron](README_Images/themes/HunterCitron.png)

> "color": { <br>
>      "border": "#315C2B", <br>
>      "text": "#315C2B", <br>
>      "outerCircleBackground": "rgba(167, 38, 8, 0.8)", <br>
>      "outerCircleBackgroundGradient":true, <br>
>      "outerCircleBackgroundGradientColor1":"rgba(24, 31, 28, 0.8)", <br>
>      "outerCircleBackgroundGradientColor2":"rgba(24, 31, 28, 0.4)", <br>
>      "innerCircleBackground": "#9EA93F", <br>
>      "selectedCategory": "#9EA93F" <br>
>    },